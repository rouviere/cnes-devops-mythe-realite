# INTRO

## slide 3

Voici une timeline de la transformation devops. Durant la présentation je vais revenir sur les + et les - de chaque thématique. En haut, on peut voir le nombre de personnes et de machines linux dans le parc. Nous avons également des PCs dit bureautiques windows.
GIT
Dans mon équipe la transformation a commencé en 2015 par la migration vers Git. Grosse étape, nous y reviendrons.
CI
Puis est venu l'intégration continue, passage à jenkins 2 et ses multibranch-pipeline-jobs et gitlab-ci plus récement
ADMIN
Au niveau de la gestion du PARC, au début tout à la main avec des scripts bash, et quand c'est devenu ingérable mise en place de ansible.
VIRT
Au niveau des technos de virtualisation, VMs et Docker. Plus récement on tente l'expérience cloud. Ce n'est pas forcément facile dans un grand groupe comme airbus DS. On en reparle plus tard

# GIT

## slide 6

Ca peut paraître etonnant aujourd'hui, mais en 2015, gérer son code son git n'était pas encore une évidence. Il a fallut se battre pour sortir de SVN. Voici quelques réactions qu'il m'arrive encore de rencontrer.

## slide 7

Avant de faire la migration SVN => Git il faut préparer.
préparer les projets: penser la découpe en repo plus petits
préparer les gens: faire des introductions à git
mise en place progressive, quand c'est possible
scripter la migration !

## slide 8

Le jour de la migration, aucun soucis.
Le script a tourné comme la veille.
Nous ne sommes plus JAMAIS retourné sous SVN.

# IC

## slide 12

Pour l'intégration continue, même combat.
Il y a une forte tradition de tests manuels chez nous qui n'est pas facile à combattre.
Il y a la peur de ne pas tester aussi bien par rapport à des tests manuels. Il faut rassurer, les tests auto vont bien plus loin qu'un test manuel.
Concernant les couts, si on limite l'ambition à tester 80% du code le gain de tester à chaque commit grandit plus on a de developpeurs et plus on fait de release.

## slide 13

Nous avons changé de système plusieurs fois. passant de jenkins1 à jenkins2, puis à gitlab ci.
Je pourrais revenir sur les raisons du passage à gitlab-ci si vous avez des questions
Il y a la peur de ne pas tester aussi bien par rapport à des tests manuels. Il faut rassurer, les tests auto vont bien plus loin qu'un test manuel.

Concernant les couts, si on limite l'ambition à tester 80% du code le gain de tester à chaque commit grandit plus on a de developpeurs et plus on fait de release.

## slide 14

Avant de mettre en place l'intégration continue il faut préparer.
préparer les projets: penser les pipelines
préparer les gens: faire des présentation techniques
mise en place progressive: passer par des projets template

## slide 16

Nous utilisons un pipeline basique, les différentes phases ont été ajoutées au fur et à mesure.
Les plus complexes ont été les phases de tests d'intégration qui nous a obligé à créer un outil nommé Pulsar pour simulerles sous systèmes.
Et la phase de déploiement.

## slide 18

Au final, l'intégration continue c'est devenu un standard.
Un projet sans CI est voué à tomber en panne dès qu'on arrête de le regarder.
On arrête jenkins, certains projets tournent encore dessus, mais on ne ne maintient plus.

# ART

## slide 20

Pour la partie gestion de binaires, c'est plus simple.
Nous etions sur nexus pour héberger les artefacts java. Nexus c'est bien, mais c'est pas super solide. Nexus 2 ne gère pas NPM / DOCKER, ensuite nous sommes passés à nexus 3. Mais nexus 3 a pas mal de petits soucis également. Ce qui a mis tout le monde d'accord, c'est Artifactory.

## slide 21

Quand on pense son gestionnaire de binaire il faut voir loin.
Séparer un repo par projet a minima.


## slide 24

Artifactory est en place, mais dur de ne plus utiliser les anciennes zone.
Beaucoup de projets/CI pointent encore dessus.
Dur de passer d'un monde open à un monde authentifié.

# ADM

## slide 26

Pour la gestion du parc informatique, il a fallu s'équiper.
Au debut nous sommes partis sur du Kickstart et du bash.
Voyant le nombre de machine augmenter j'ai du vite m'orienter vers un outil plus industriel, mon choix s'est porté sur ansible et python.
Ce fut un choix payant. même si il a fallut un peu se battre.

# DEP

## slide 33

Coté virtualisation/déploiement, le terrain est vierge.
certes au début il y eu un doute sur l'interet de virtualiser, 
mais aujourd'hui, plus de doute.
Concernant docker, c'est plus délicat. Les projets migrent plus par
la pression de l'effet de mode que par volonté.
En tout cas, pour le devops, mis à part la sécurité, c'est le pied.

## slide 35

le monde du container ca bouge....vite. Il faut rester concentré.

# CONCL

## slide 37

Pour conclure, après 5 ans nous avons réussi à négocier tous ces changements.
La forge actuelle nous permet d'affronter les defis de productivités.

