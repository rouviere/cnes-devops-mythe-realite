function init() {
    // Autoescaping html tags in pre code blocks
    // code found on https://www.raymondcamden.com/2013/04/25/Autoescaping-code-blocks-in-Revealjs/
    // this code is unable to escape some tags: html/body/head
    var cblocks = document.querySelectorAll("pre code.html");
    if (cblocks.length) {
        for (var i = 0, len = cblocks.length; i < len; i++) {
            var dom = cblocks[i];
            var html = dom.innerHTML;
            html = html.replace(/</g, "&#60;").replace(/>/g, "&#62;");
            dom.innerHTML = html;
        }
    }

    // More info about config & dependencies:
    // - https://github.com/hakimel/reveal.js#configuration
    // - https://github.com/hakimel/reveal.js#dependencies
    Reveal.initialize({
        controls: true,
        progress: true,
        history: true,
        center: true,
        // Display the page number of the current slide
        slideNumber: true,
        theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
        // Optional libraries used to extend on reveal.js
        dependencies: [{
                src: "lib/js/classList.js",
                condition: function () {
                    return !document.body.classList;
                }
            },

            {
                src: "plugin/markdown/marked.js",
                condition: function () {
                    return !!document.querySelector("[data-markdown]");
                }
            },
            {
                src: "plugin/markdown/markdown.js",
                condition: function () {
                    return !!document.querySelector("[data-markdown]");
                }
            },
            // Syntax highlight for <code> elements
            {
                src: "plugin//highlight/highlight.js",
                async: true,
                callback: function () {
                    hljs.initHighlightingOnLoad();
                }
            },
            {
                src: "plugin/notes/notes.js"
            }
        ]
    });
}
// override default reveal load by providing our own init function
window.addEventListener("DOMContentLoaded", init, false);

// Enable navigation with mouse
function handleClick(e) {
    e.preventDefault();
    // left click ---> move next
    if (e.button === 0) Reveal.next();
    //right click ---> move previous
    if (e.button === 2) Reveal.prev();
}

function enableMouseNavigation() {
    console.log("Activation de la navigation souris !");
    window.addEventListener("mousedown", handleClick, false);
    window.addEventListener(
        "contextmenu",
        function (e) {
            e.preventDefault();
        },
        false
    );
}