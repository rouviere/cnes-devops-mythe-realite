// La construction des graphs git

var myTemplateConfig = {
    branch: {
        lineWidth: 8,
        spacingX: 50,
        showLabel: true,                  // display branch names on graph

    },
    commit: {
        spacingY: -60, // the vertical space between two dots
        dot: {
            size: 20,
            font: "normal 15pt monospace white",
        },

        message: {
            displayAuthor: false,
            displayBranch: false,
            displayHash: false,
            font: "normal 16pt monospace",

        },
        shouldDisplayTooltipsInCompactMode: false, // default = true
        tooltipHTMLFormatter: function (commit) {
            return "" + commit.sha1 + "" + ": " + commit.message;
        }
    }
};
var myTemplate = new GitGraph.Template(myTemplateConfig);

/**
 * return a commit options where Dot message has the sha1
 * @param {*} options
 */
var C = function (options) {
    options.commitDotText = options.sha1;
    options.commitDotTextColor = "white";
    return options;
}

/**
 *
 * @param {*} elementId The canvas element id to draw the grap
 * @param {*} mode      The mode among "compact" and "extended"
 * @param {*} template  The template, myTemplate byd efault
 */
var GG = function (elementId, mode, template) {
    var options = {
        elementId: elementId,
        reverseArrow: true,
        orientation: "vertical-reverse",
        template: myTemplate,
        mode: mode
    };
    if (mode != undefined) {
        options.mode = compact;
    }
    if (template != undefined) {
        options.template = template;
    }
    return new GitGraph(options);
}

//////////////
//git_graph_1
//////////////
var br_dev = GG("git_graph_1").branch("develop");
br_dev.commit(C({ sha1: "0", message: "r00t" }));
br_dev.commit(C({ sha1: "A", message: "implement feature" }));
br_dev.commit(C({ sha1: "B", message: "forgot ';'" }));

var br_feat = br_dev.branch("feat");
br_feat.commit(C({ sha1: "C", message: "cool feature" }));

br_dev.commit(C({ sha1: "D", message: "fix build" }));
br_feat.merge(br_dev, C({ sha1: "E", message: "retrieve fix" }));

//////////////
//git_graph_2
//////////////
var br_dev = GG("git_graph_2").branch("develop");
br_dev.commit(C({ sha1: "0", message: "r00t" }));
br_dev.commit(C({ sha1: "A", message: "implement feature" }));
br_dev.commit(C({ sha1: "B", message: "fix build" }));

