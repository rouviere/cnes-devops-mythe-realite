# devops mythe ou réalité

Les slides pour la présentation [DEVOPS CNES](https://www.comet-cnes.fr/evenements/devops-mythe-ou-realite)

Speaker: Nicolas Rouviere (nicolas.rouviere@airbus.com)

- Retrouvez les slides publiés [ICI](https://rouviere.frama.io/cnes-devops-mythe-realite/#/)
- Téléchargez les slides en TAR.GZ [ICI](https://framagit.org/rouviere/cnes-devops-mythe-realite/-/jobs)
- Téléchargez le PDF [ICI](https://framagit.org/rouviere/cnes-devops-mythe-realite/raw/master/dist/devops.pdf?inline=false)

